# Information about enviroment:
- This is a REST Backend of an Android App named "ExplorersGuide" for the "mobile computing 2" module.
- It has an idividual db infrastructure for its solution. You can find it in the "init.sql" script.

# Frontend

The frontend part of this project can be found in this [Gitlab-Repository](https://gitlab.com/jhkloss/explorers-guide).

# Technologies
- Quarkus service with Java as main language and access to a database.
- Junit during tests.
- Mysql database for storage.
- Traefik reverse proxy to handle incoming traffic and routing
- A Docker Compose configuration to start everything
- Grafana and Prometheus for metrics (http://localhost/metrics-ui/)
- Jaeger for tracing

# Api entry points
- @Path("/backend"): root

- @GET @Path("/db/connection/"): returns the actual connection state from backend to db.

- @GET @Path("/table/{name}"): returns an whole db table. Request example: localhost/backend/table/Users

- @POST @Path("/testing/user/delete/"): The intention of this @POST api call is just for testing reason. Its delete an existing user. You need to hand over the usermail and the password as a json.

- @POST @Path("/user/login/"): This @POST api call takes a json in form as a Userlogin (see UserLogin.java). With that information in this json it will try to search after an entry in the db and will compare the values. (email, password) After the compare it will return an success or a failure.

- @POST @Path("/user/registration/"): This @POST api call takes a json in form as a UserRegistration (see UserRegistration java). With that information in this json it will try to add the entry in the db.user table. (username, email, password) After this try it can be the success case that the user get added in the db or the failure case with username/email already exists.

# Start the enviroment in vs-code:
- Execute all terminal commands in root folder of this repo.

- After cloning the project, at first clean and generate a package into the target folder: 
```mvn clean package -Dquarkus.package.type=mutable-jar -DskipTests```

- When no errors with the maven comes up then you can start the enviroment (before executing this read the "Need to know" section and be sure that these points are recognized and fulfilled): 
```docker-compose up```

- (Optional for hotfixes in container while runtime) Start remote dev over maven with: (For more info: https://blog.sebastian-daschner.com/entries/quarkus-remote-dev-in-containers)
```mvn quarkus:remote-dev -Ddebug=false -Dquarkus.package.type=mutable-jar -Dquarkus.live-reload.url=http://localhost/backend -Dquarkus.live-reload.password=123```

# Need to know
- While live-dev is on you can change the java code and it will push the changes directly to the container. 
- docker-compose will generate a couple of containers. (See in "docker-compose.yml" under "service:" wich containers)
- It will also generate an network for the container. Over this network the containers communicate
- In the mysql folder you can find some folder and files:
	- "/db-data" that local folder is mounted to the "db-container" and saves the past state of the db.
	- "init.sql" is the initialization script for the db and will executed when the "db-data" folder dont exist.
	- If you want to change the db you need to delete the "db-data" folder. After that you can change the init.db file and the changes will be applied at the next docker-compose up.
- Its necessary that the "$PWD" variable is setted in linux and in windows you need to change the "$PWD" occures in the docker-compose file with "%cd%". Otherwise an docker-compose up is not possible.
