package org.explorersguide.api.services;
import java.sql.SQLException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.explorersguide.api.requests.UserLogin;
import org.explorersguide.api.requests.UserRegistration;
import org.explorersguide.systems.DataBaseManagement;

/**
 * Represents a Sevice that just takes incoming api call and forwads that to the 
 * resonsible class. In this case: DataBaseManagement.java class.
 * It can be addressed over the path "/backend" (configured in treafik).
 * Every api endpoint is traced by the time and the count of the requests. Here it gets monitored over Prometheus and Grafana.
 */
@Path("/")
@ApplicationScoped
public class ForwarderService {

    // To get the uri info in some apicalls
    @Context
    private UriInfo uriInfo;

    // Inject DataBaseManagement class to trigger the functions when an apicall comes in
    @Inject
    DataBaseManagement dataBaseManagement;
    
    /**
     * returns the actual connection state from backend to db.
     * 
     * @return                  true - connection can be etablished. false - cant etablish a connection.
     */
    @GET
    @Path("/db/connection/")
    @Produces(MediaType.TEXT_PLAIN)
    @Counted(name = "DB connection requests")
    @Timed(name = "Time to deliver DB connection", unit = MetricUnits.MILLISECONDS)
    public boolean checkDbConnection() {

        return dataBaseManagement.connectToDB();
    }

    /**
     * returns an whole db table. Request example: localhost/backend/table/Users
     * 
     * @param name              table name to be searched for. 
     * @return                  a json wich represents the table. When tablename wasnt found it returns this as a json either. Example: {"success":false, "message":"Table 'explorersguide_db.test' doesn't exist"}.
     * @throws SQLException     at a unexpected sql error. 
     */
    @GET
    @Path("/table/{name}") 
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "GetTable() requests")
    @Timed(name = "Time to deliver GetTable()", unit = MetricUnits.MILLISECONDS)
    public Response getTable(@PathParam("name") String name) throws SQLException{

        String tableResponse = dataBaseManagement.getTable(name);

        return Response.ok(tableResponse, MediaType.APPLICATION_JSON).build(); 
    }

    /**
     * The intention of this @POST api call is just for testing reason. 
     * Its delete an existing user. You need to hand over the usermail and the password as a json. 
     * 
     * @param userLogin         the jsonobject that represent a userlogin. It contains an email and a password.
     * @return                  a json where the information is listed if the user can be deleted or not.
     * @throws SQLException     at a unexpected sql error. 
     */
    @POST
    @Path("/testing/user/delete/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "deleteUser() requests")
    @Timed(name = "Time to deliver deleteUser()", unit = MetricUnits.MILLISECONDS)
    public Response deleteUser(UserLogin userLogin) throws SQLException{ 

        String deleteUserResponse = dataBaseManagement.deleteUser(userLogin);
        
        return Response.ok(deleteUserResponse, MediaType.APPLICATION_JSON).build();
    }
    
    /**
     * This @POST api call takes a json in form as a Userlogin (see UserLogin.java). 
     * With that information in this json it will try to search after an entry in the db and will compare the values. (email, password)
     * After the compare it will return an success or a failure.
     * 
     * @param userLogin         the jsonobject that represent a userlogin. It contains an email and a password.
     * @return                  a json with the information if the user login was successfully or not. Success - it will return the user row in the db. Failure - it will return an success = false.
     * @throws SQLException     at a unexpected sql error. 
     */
    @POST
    @Path("/user/login/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "login() requests")
    @Timed(name = "Time to deliver login()", unit = MetricUnits.MILLISECONDS)
    public Response login(UserLogin userLogin) throws SQLException{ 

        String loginResponse = dataBaseManagement.loginUser(userLogin);
        
        return Response.ok(loginResponse, MediaType.APPLICATION_JSON).build();
    }

    /**
     * This @POST api call takes a json in form as a UserRegistration (see UserRegistration.java).
     * With that information in this json it will try to add the entry in the db.user table. (username, email, password)
     * After this try it can be the success case that the user get added in the db or the failure case with username/email already exists.
     * 
     * @param userRegistration  the jsonobject that represent a UserRegistration. It contains an email, an username and a password.
     * @return                  a json with the information if the user registration was successfully or not. Success - success : true. Failure - success : false, message : "error message".
     * @throws SQLException     at a unexpected sql error. 
     */
    @POST
    @Path("/user/registration/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "registrate() requests")
    @Timed(name = "Time to deliver registrate()", unit = MetricUnits.MILLISECONDS)
    public Response registrate(UserRegistration userRegistration) throws SQLException{

        String response = dataBaseManagement.registrateUser(userRegistration);
        
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }

}   