package org.explorersguide.systems;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.explorersguide.api.requests.UserLogin;
import org.explorersguide.api.requests.UserRegistration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;

/**
 * Represents a singleton for that get injected by the ForwaderService class.
 * It have an access to the db, can call queries, can add entries, can delete entries and all other db calls.
 * It have also functions like hash and unhash to compare and generate strings. 
 */
@ApplicationScoped
public class DataBaseManagement {
    
    // Represents the connection to the db. After every function call it gets diconnected. Must be connected to do calls on db.
    private Connection connection = null;

    // All neccesarry db variables to connect.
    private static String dbName            = System.getenv("DB_NAME"); //"explorersguide_db";             
    private static String dbContainerName   = System.getenv("DB_CONTAINERNAME"); //"explorersguide_mysql_db";
    private static String dbPort            = System.getenv("DB_PORT"); //"3306";
    private static String dbUser            = System.getenv("DB_USER"); //"root";
    private static String dbPassword        = System.getenv("DB_PASSWORD"); //"mysql";

    /**
     * Tries to connect to the db with the variables setted above.
     * 
     * @return true - connected, false - cant connect. 
     */
    public boolean connectToDB(){

        try { 
            connection = DriverManager.getConnection("jdbc:mysql://" + dbContainerName + ":" + dbPort +"/" + dbName, dbUser, dbPassword);
            System.out.println("Connection to db sucessfully");
            System.out.println("DB Connection: " + connection);
            
        } catch (SQLException ex)
        {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            
            return false;
        }

        return true;
    }

    /**
     * Tries to registrate an user to the db. Before add the password to the db it gets hashed.
     * 
     * @param userRegistration  the user with all the information for the db.call to add it to the db.
     * @return                  1) {"success" : true, "message" : "user registrated"}, 
     *                          2) {"success" : false, "message" : "Cant connect to db" or "username already exists" or "email already exists" or "uknown error at user registration insert" or <SQLException>)}
     * @throws SQLException     at a unexpected sql error.
     */
    public String registrateUser(UserRegistration userRegistration) throws SQLException {

        JSONObject jsonObject = new JSONObject();

        
        // First we need to connect to the db. At failure return with error.
        if (!connectToDB()) {
            jsonObject.put("success",false);
            jsonObject.put("message", "Cant connect to db");

            return jsonObject.toString();
        }


        // Foreign keys for user insert
        int userRank = 0;
        int userRole = 0;

        // Get Foreign keys
        try (Statement stmt = connection.createStatement()) {
            
            ResultSet rsUserRole = stmt.executeQuery("SELECT * FROM " + dbName + ".Role WHERE description = \"RegisteredUser\" LIMIT 1");
            rsUserRole.next();
            userRole = rsUserRole.getInt("idRole");

            ResultSet rsUserRank = stmt.executeQuery("SELECT * FROM " + dbName + ".Rank WHERE description = \"Unranked\" LIMIT 1");
            rsUserRank.next();
            userRank = rsUserRank.getInt("idRank");
            
        } catch (SQLException ex) {
            connection.close();

            jsonObject.put("success",false);
            jsonObject.put("message", ex.getMessage());

            return jsonObject.toString();
        }


        // Insert User to DB
        try (PreparedStatement preparedStmt = connection.prepareStatement("INSERT INTO explorersguide_db.User (`username`, `email`, `password`, `rank`, `role`) VALUES (?, ?, ?, ?, ?)")){ 
            preparedStmt.setString(1, userRegistration.getUsername());
            preparedStmt.setString(2, userRegistration.getEmail());
            preparedStmt.setString(3, hashPassword(userRegistration.getPassword()));
            preparedStmt.setInt(4, userRank);
            preparedStmt.setInt(5, userRole);
            
            preparedStmt.execute();

        } catch (SQLException ex){
            String message = ex.getMessage();
            String response = "uknown error at user registration insert";

            if (message.contains("username") && message.contains("Duplicate")){
                response = "username already exists";
            }
            else if (message.contains("email") && message.contains("Duplicate")) {
                response = "email already exists";
            }

            connection.close();
            
            jsonObject.put("success", false);
            jsonObject.put("message", response);

            return jsonObject.toString();
        }


        connection.close();
        
        jsonObject.put("success", true);
        jsonObject.put("message", "user registrated");

        return jsonObject.toString();
    }

    /**
     * Tries to login an user to the db. Should be saved in production mode!
     * 
     * @param userLogin         the user with all the information for the db.call to compare it in the db.
     * @return                  1) {"success" : true, "message" : <user row in the db>}, 
     *                          2) {"success" : false, "message" : "Cant connect to db" or "Email dont match with password or User dont exist" or <SQLException>)}                 
     * @throws SQLException     at a unexpected sql error.
     */
    public String loginUser(UserLogin userLogin) throws SQLException {

        JSONObject jsonObject = new JSONObject();


        // First we need to connect to the db. At failure return with error.
        if (!connectToDB()) {
            jsonObject.put("success",false);
            jsonObject.put("message", "Cant connect to db");

            return jsonObject.toString();
        }
        

        // Get user from db with the email and compare the password for it with the userLogin.password.
        String stmtString = "SELECT * FROM " + dbName + ".User WHERE email = \"" + userLogin.getEmail() + "\""; 
        try (Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {         
            
            ResultSet rsUser = stmt.executeQuery(stmtString);
            rsUser.last();          
            int count = rsUser.getRow();    

            if (count > 0 && compareHashedPassword(userLogin.getPassword(), rsUser.getString("password"))){
                
                List<String>    columns         = loadColumns(connection, "User");
                JSONArray       array           = new JSONArray();
                
                JSONObject record = new JSONObject();
                for (String column : columns) {
                    record.put(column, rsUser.getObject(column));
                }
                array.put(record);
                        
                jsonObject.put("user", array);
                jsonObject.put("success", true);

                connection.close();

                return jsonObject.toString();
            }
            else {
                connection.close();
                jsonObject.put("success", false);
                jsonObject.put("message", "Email dont match with password or User dont exist");
                
                return jsonObject.toString();
            }

        } catch (SQLException ex) {
            connection.close();
            jsonObject.put("success", false);
            jsonObject.put("message", ex.getMessage());

            return jsonObject.toString();
        }
    }
   
    /**
     * Returns an whole db.table as a json string.
     * 
     * @param tableName         table name for the table that was asked for.
     * @return                  1) {"success": true, <tableName>: [{...}]}
     *                          2) {"success": false, "message": "Table 'explorersguide_db.<tableName>' doesn't exist" or "Cant connect to db" or <SQLException>}
     * @throws SQLException     at a unexpected sql error.
     */
    public String getTable(String tableName) throws SQLException{

        JSONObject jsonObject = new JSONObject();

        // First we need to connect to the db. At failure return with error.
        if (!connectToDB()) {
            jsonObject.put("success",false);
            jsonObject.put("message", "Cant connect to db");

            return jsonObject.toString();
        }

        JSONArray array = new JSONArray();
        
        // Load all cloumns and the data for the whole table and return it as json string.
        try {

            List<String>    columns         = loadColumns(connection, tableName);
            ResultSet       dataSet         = loadData(connection, tableName);
        
            while (dataSet.next()) {
                JSONObject record = new JSONObject();

                for (String column : columns) {
                    record.put(column, dataSet.getObject(column));
                }
                array.put(record);
            }
    
        } catch (SQLException ex){
            jsonObject.put("success",false);
            jsonObject.put("message", ex.getMessage());

            return jsonObject.toString();
        }
        jsonObject.put("success", true);
        jsonObject.put(tableName, array);
        
        connection.close();

        return jsonObject.toString();
    }

    /**
     * Deletes a given user in the db.
     * 
     * @param userLogin         the user with all the information for the db.call to search after it.
     * @return                  1) {"success": true, "message": "user deleted"}
     *                          2) {"success": false, "message": "Cant connect to db" or <SQLException>}
     * @throws SQLException     at a unexpected sql error.
     */
    public String deleteUser(UserLogin userLogin) throws SQLException {
        
        JSONObject  jsonObject  = new JSONObject();
        String      userJson    = loginUser(userLogin);

        // First we need to connect to the db. At failure return with error.
        if (!connectToDB()) {
            jsonObject.put("success",false);
            jsonObject.put("message", "Cant connect to db");

            return jsonObject.toString();
        }

        JSONObject  jsonObjectUser  = new JSONObject(userJson);

        // If a user was not found.
        if (!jsonObjectUser.getBoolean("success"))
        { 
            connection.close();

            return userJson;
        }

        int idUser = jsonObjectUser.getJSONArray("user").getJSONObject(0).getInt("idUser");

        try (PreparedStatement preparedStmt = connection.prepareStatement("DELETE FROM " + dbName + ".User WHERE idUser = " + idUser)) {
            
            preparedStmt.execute();
            
        } catch (SQLException ex) {
            connection.close();

            jsonObject.put("success",false);
            jsonObject.put("message", ex.getMessage());

            return jsonObject.toString();
        }

        connection.close();

        jsonObject.put("success", true);
        jsonObject.put("message", "user deleted");

        return jsonObject.toString();
    }

    /**
     * Hashes a given password. Extern package was used for this.
     * 
     * @param password          the password what should be hashed 
     * @return                  Hashed password as String
     */
    public String hashPassword(String password) {

        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    /**
     * Compares a hashed string with an plaintext string. It will hash the plantext and compare it then in the internal function. External package was used.
     * 
     * @param plainTextPassword plain password
     * @param hashedPassword    hashed password
     * @return                  true - equal, false - not equal
     */
    public boolean compareHashedPassword(String plainTextPassword, String hashedPassword) {

        return BCrypt.checkpw(plainTextPassword, hashedPassword);
    }

    /**
     * Loads all columns for a table in the db and returns it as a list
     * 
     * @param connection        connection to the db
     * @param tableName         tablename
     * @return                  returns all columns for a table as List<String>
     * @throws SQLException     at a unexpected sql error.
     */
    public static List<String> loadColumns(Connection connection, String tableName) throws SQLException {
        
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT COLUMN_NAME FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE TABLE_NAME LIKE '" + tableName + "'");
        
        List<String> columnsName = new ArrayList<String>();
        
        while(resultSet.next()) {
            columnsName.add(resultSet.getString("COLUMN_NAME"));
        }
        
        return columnsName;
    }

    /**
     * Loads all data for a table in the db and returns it as a ResultSet
     * 
     * @param connection        connection to the db
     * @param tableName         tablename
     * @return                  returns all rows for a table as a sql.ResultSet
     * @throws SQLException     at a unexpected sql error.
     */
    public static ResultSet loadData(Connection connection, String tableName) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM " + dbName + "." + tableName +"");
        
        return resultSet;
    }
 
    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}