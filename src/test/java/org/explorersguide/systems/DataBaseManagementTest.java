package org.explorersguide.systems;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import javax.inject.Inject;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import io.quarkus.test.junit.QuarkusTest;

/**
 * This test class is just testing the DataBaseManagement helper functions. 
 * The db related functions are tested in ForwarderServiceTest.java.
 */
@QuarkusTest()
@TestMethodOrder(OrderAnnotation.class)
public class DataBaseManagementTest {
    
    //Inject DataBaseManagement singleton for testing the functions
    @Inject
    DataBaseManagement dataBaseManagement;

    /**
     * This test is testing the hashPassword and compareHashedPassword functions.
     * Fails at password == hashedPassword, compareHashedPassword(pasword, hashedPassword) != true,
     * compareHashedPassword(pasword, secondHashedPassword) != true, compareHashedPassword(wrongPassword, hashedPassword) == true;
     * compareHashedPassword(wrongPassword, secondHashedPassword) == true, compareHashedPassword(pasword, hashedWrongPassword) == true
     */
    @Test
    @Order(1)
    public void testHashPassword(){

        String pasword                  = "Password";
        String hashedPassword           = dataBaseManagement.hashPassword(pasword);
        String secondHashedPassword     = dataBaseManagement.hashPassword(pasword);
        
        String wrongPassword            = "password";
        String hashedWrongPassword      = dataBaseManagement.hashPassword(wrongPassword);

        assertNotEquals(pasword, hashedPassword);
        assertNotEquals(hashedPassword, secondHashedPassword);
        assertEquals(true, dataBaseManagement.compareHashedPassword(pasword, hashedPassword));
        assertEquals(true, dataBaseManagement. compareHashedPassword(pasword, secondHashedPassword));
        assertEquals(false, dataBaseManagement.compareHashedPassword(wrongPassword, hashedPassword));
        assertEquals(false, dataBaseManagement.compareHashedPassword(wrongPassword, secondHashedPassword));
        assertEquals(false, dataBaseManagement.compareHashedPassword(pasword, hashedWrongPassword));
    }
    
}
