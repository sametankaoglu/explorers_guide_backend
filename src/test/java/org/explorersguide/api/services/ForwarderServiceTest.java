package org.explorersguide.api.services;

import io.quarkus.test.junit.QuarkusTest;

import org.explorersguide.api.requests.UserLogin;
import org.explorersguide.api.requests.UserRegistration;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.Gson;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the ForwarderService class.
 * Test a full scenario with registration, login, delete user and all fail inputs.
 * Important is it to start the test in the @Order(1-n) or it will fail at the second execution
 */
@QuarkusTest()
@TestMethodOrder(OrderAnnotation.class)
public class ForwarderServiceTest {
  
  // All necessary variables for a Test User 
  private static String rootUrlToBackend  = "http://localhost/backend/";
  private static String userName          = "KeyboardWarrior";
  private static String userEmail         = "MaxTheMustermann@gmail.com";
  private static String userPassword      = "HabeMehrKreditKartenAlsMaxMustermann";

    /**
     * Tests an succesfully user registration api call.
     * Fails at connection != 200 and user cant be registrated (success == false).
     * @throws IOException unexpected exception
     */
    @Test
    @Order(1)
    public void testNewUserRegistration() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "user/registration/";
      URL     url               = new URL(urlForPOSTRequest);

      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);

      // Create the request body
      UserRegistration    user    = new UserRegistration(userName, userEmail, userPassword);
      Gson                gson    = new Gson();
      String              message = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(message);
      osw.flush();
      osw.close();

      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      if (status != 200) connection.disconnect();
      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      // Check if registration was successfully
      JSONObject  jsonObject  = new JSONObject(responseContent.toString());
      boolean     success     = jsonObject.getBoolean("success");
      assertEquals(true, success);
      
      connection.disconnect();
    }

    /**
     * Tests an existing username registration api call. 
     * Fails at connection != 200 and user can be registrated (success != false).
     * @throws IOException unexpected exception
     */
    @Test
    @Order(2)
    public void testExistingUserNameRegistration() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "user/registration/";
      URL     url               = new URL(urlForPOSTRequest);


      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);


      // Create the request body
      UserRegistration    user    = new UserRegistration(userName, "different@gmail.com", userPassword);
      Gson                gson    = new Gson();
      String              apiMessage = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(apiMessage);
      osw.flush();
      osw.close();


      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      if (status != 200) connection.disconnect();
      assertEquals(200, status);


      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();


      // Check if registration was successfully
      JSONObject  jsonObject    = new JSONObject(responseContent.toString());
      
      boolean     success       = jsonObject.getBoolean("success");
      assertEquals(false, success);

      String      message       = jsonObject.getString("message");
      assertEquals("username already exists", message);
    
      connection.disconnect();
    }

    /**
     * Tests an existing useremail registration api call. 
     * Fails at connection != 200 and user can be registrated (success != false).
     * @throws IOException unexpected exception
     */
    @Test
    @Order(3)
    public void testExistingEmailRegistration() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "user/registration/";
      URL     url               = new URL(urlForPOSTRequest);

      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);

      // Create the request body
      UserRegistration    user    = new UserRegistration("different", userEmail, userPassword);
      Gson                gson    = new Gson();
      String              apiMessage = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(apiMessage);
      osw.flush();
      osw.close();

      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      if (status != 200) connection.disconnect();
      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      // Check if registration was successfully
      JSONObject  jsonObject    = new JSONObject(responseContent.toString());
      boolean     success       = jsonObject.getBoolean("success");
      String      message  = jsonObject.getString("message");
      assertEquals(false, success);
      assertEquals("email already exists", message);

      connection.disconnect();
    }

    /**
     * Tests a successfully user login api call. 
     * Fails at connection != 200 and user cannot log in. (success == false).
     * @throws IOException unexpected exception
     */
    @Test
    @Order(4)
    public void testSuccessfullyUserLogin() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "user/login/";
      URL     url               = new URL(urlForPOSTRequest);

      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);

      // Create the request body
      UserLogin           user    = new UserLogin(userEmail, userPassword);
      Gson                gson    = new Gson();
      String              message = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(message);
      osw.flush();
      osw.close();

      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      // Check if login was successfully
      JSONObject  jsonObject  = new JSONObject(responseContent.toString());
      
      boolean     success     = jsonObject.getBoolean("success");
      assertEquals(true, success);

      String      email       = jsonObject.getJSONArray("user").getJSONObject(0).getString("email"); 
      assertEquals(userEmail, email);
      
      connection.disconnect();
    }

    /**
     * Tests a failed user login api call. (wrong password)
     * Fails at connection != 200 and user can log in (success != false). 
     * @throws IOException unexpected exception
     */
    @Test
    @Order(5)
    public void testFailedUserLogin() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "user/login/";
      URL     url               = new URL(urlForPOSTRequest);

      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);

      // Create the request body
      UserLogin           user    = new UserLogin(userEmail, "wrongPassword");
      Gson                gson    = new Gson();
      String              message = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(message);
      osw.flush();
      osw.close();

      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      // Check if login was successfully
      JSONObject  jsonObject  = new JSONObject(responseContent.toString());
      boolean     success     = jsonObject.getBoolean("success");
      assertEquals(false, success);
      
      connection.disconnect();
    }

    /**
     * Tests a getTable api call. With User as parameter.
     * Fails at connection != 200 and userEmail != getTable().getUser().getEmail(). 
     * @throws IOException unexpected exception
     */
    @Test
    @Order(6)
    public void testGetUserTable() throws IOException {
      
      URL url = new URL(rootUrlToBackend+"table/User");
      
      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);

      int status = connection.getResponseCode();
      
      if (status != 200) {
        connection.disconnect();
      }

      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      // Check if get table was successfully.
      JSONObject  jsonObject  = new JSONObject(responseContent.toString());
      String      email       = "empty"; 

      // Go through table and take the user inserted before in the test: "TestUserRegistration".
      JSONArray users = jsonObject.getJSONArray("User");
        for (int i = 0; i < users.length(); i++) {
            email = users.getJSONObject(i).getString("email");
            if (email.equals(userEmail)) break;
        }

      assertEquals(userEmail, email);

      connection.disconnect();    
    }

    /**
     * Tests to delete a user api call. 
     * Fails at connection != 200 and user dont deleted (success == false). 
     * @throws IOException unexpected exception
     */
    @Test
    @Order(7)
    public void testDeleteUser() throws IOException {
      String  urlForPOSTRequest = rootUrlToBackend + "testing/user/delete/";
      URL     url               = new URL(urlForPOSTRequest);

      // Request setup
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("POST");
      connection.setConnectTimeout(5000);
      connection.setReadTimeout(5000);
      connection.setRequestProperty("Content-Type", "application/json; utf-8");
      connection.setRequestProperty("Accept", "application/json");
      connection.setDoOutput(true);

      // Create the request body
      UserLogin           user    = new UserLogin(userEmail, userPassword);
      Gson                gson    = new Gson();
      String              message = gson.toJson(user);
      OutputStream        ostream = connection.getOutputStream();
      OutputStreamWriter  osw     = new OutputStreamWriter(ostream);

      osw.write(message);
      osw.flush();
      osw.close();

      // Check if a connection could be etablished
      int status = connection.getResponseCode();
      assertEquals(200, status);

      // Get response
      String          line;
      StringBuffer    responseContent   = new StringBuffer();
      BufferedReader  reader            = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      
      while ((line = reader.readLine()) != null){
        responseContent.append(line);
      }
      reader.close();

      System.out.println(responseContent.toString());
      
      // Check if delete was successfully
      JSONObject  jsonObject  = new JSONObject(responseContent.toString());
      boolean     success     = jsonObject.getBoolean("success");
      assertEquals(true, success);

      connection.disconnect();
    }

}